﻿using Microsoft.JSInterop;

namespace ChipBot.Client.Services
{
    public class ThemeManager : IAsyncDisposable
    {
        private IJSObjectReference js;
        private readonly IJSRuntime jsRuntime;

        public ThemeManager(IJSRuntime jsRuntime)
            => this.jsRuntime = jsRuntime;

        public async Task SetTheme(string theme)
        {
            await WaitForReference();
            await js.InvokeVoidAsync("setTheme", theme);
        }

        public async Task<string> SetCurrentTheme()
        {
            await WaitForReference();
            return await js.InvokeAsync<string>("setCurrentTheme");
        }

        public async Task<string> GetCurrentTheme()
        {
            await WaitForReference();
            return await js.InvokeAsync<string>("getCurrentTheme");
        }

        private async Task WaitForReference()
            => js ??= await jsRuntime.InvokeAsync<IJSObjectReference>("import", $"/js/themeManager.js");

        public async ValueTask DisposeAsync()
        {
            if (js != null)
                await js.DisposeAsync();
            GC.SuppressFinalize(this);
        }

        public static List<Theme> Themes { get; } = new List<Theme>()
        {
            new()
            {
                Name = "Auto",
                Value = "auto",
                Icon = "bi bi-circle-half"
            },
            new()
            {
                Name = "Dark",
                Value = "dark",
                Icon = "bi bi-moon-stars-fill"
            },
            new()
            {
                Name = "Light",
                Value = "light",
                Icon = "bi bi-sun-fill"
            },
        };
    }

    public class Theme
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Icon { get; set; }
    }
}

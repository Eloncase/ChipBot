﻿export function init(ref) {
    const draggableElements = getDraggable();
    for (const element of draggableElements) {
        element.ondragstart = (e) => onDragStart(e, ref);
        element.ondragover = (e) => onDragOver(e, ref);
        element.ondragend = (e) => onDragEnd(e, ref);
    }
    document.getElementById("dashboardTrash").ondragover = (e) => onDragOver(e, ref);
}

async function onDragStart(ev, ref) {
    ev.dataTransfer.setData("id", ev.currentTarget.id);
    await ref.invokeMethodAsync("OnDragStart");
}

function onDragOver(ev, ref) {
    const dragId = ev.dataTransfer.getData("id");
    const targetId = ev.currentTarget.id;
    if (dragId === targetId) return;

    const elToMove = document.getElementById(dragId);
    const targetEl = document.getElementById(targetId);

    if (targetId == "dashboardTrash") {
        const trash = document.getElementById("dashboardTrash");
        trash.appendChild(elToMove);

        elToMove.setAttribute("trashed", "true");

        ev.preventDefault();
        return;
    }

    const insertToRight = ev.currentTarget.offsetWidth / 2 < ev.clientX - ev.currentTarget.offsetLeft;

    if (insertToRight) {
        targetEl.parentElement.insertBefore(targetEl, elToMove);
    } else {
        targetEl.parentElement.insertBefore(elToMove, targetEl);
    }
    elToMove.setAttribute("trashed", "false");

    ev.preventDefault();
}

async function onDragEnd(ev, ref) {
    getTrashed().forEach(el => el.remove());
    await ref.invokeMethodAsync("OnDragEnd", Array.from(getDraggable()).map(el => el.id));
}

function getDraggable() {
    return document.querySelectorAll("[draggable]");
}

function getTrashed() {
    return document.querySelectorAll("[trashed=true]");
}
﻿export function setTheme(theme) {
    if (theme === "auto") {
        removeTheme();
        return;
    }

    localStorage.setItem("appTheme", theme);
    document.documentElement.dataset.bsTheme = theme;
}

export function removeTheme() {
    localStorage.removeItem("appTheme");
    setCurrentTheme();
}

export function setCurrentTheme() {
    let theme = localStorage.getItem("appTheme");
    let themeToSet = theme && theme !== "auto" ? theme : window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
    document.documentElement.dataset.bsTheme = themeToSet;
    return theme ? theme : "auto";
}

export function getCurrentTheme() {
    return localStorage.getItem("appTheme");
}
﻿using ChipBot.Core.Abstract.Interfaces;

namespace ChipBot
{
    public class Bot : IBot
    {
        public long CreatedTS => 1495131632000;

        public string Description => "is a private-use multipurpose Discord bot.";

        public string Thumbnail => "https://i.imgur.com/b8TrBUl.png";
    }
}

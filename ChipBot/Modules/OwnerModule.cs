﻿using ChipBot.Core.Attributes;
using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;

namespace ChipBot.Modules
{
    [RegisterToDevGuildOnly]
    [RequireUserPermission(GuildPermission.Administrator)]
    [Group("owner", "owner commands")]
    public class OwnerModule : ChipInteractionModuleBase
    {
        [SlashCommand("post-message", "post message in channel")]
        public async Task PostMessage(string msg, ITextChannel channel)
        {
            await channel.SendMessageAsync(msg);
            await ReplySuccessAsync(true);
        }

        [SlashCommand("set-streaming", "set streaming a game")]
        public async Task SetActivityAsync(string url, string name)
        {
            await Context.Client.SetActivityAsync(new StreamingGame(name, url));
            await ReplySuccessAsync(true);
        }

        [SlashCommand("set-playing", "set playing a game")]
        public async Task SetActivityAsync(string activity)
        {
            await Context.Client.SetGameAsync(activity);
            await ReplySuccessAsync(true);
        }

        [SlashCommand("set-status", "set user status")]
        public async Task SetStatusAsync(string userStatus)
        {
            await Context.Client.SetCustomStatusAsync(userStatus);
            await ReplySuccessAsync(true);
        }
    }
}

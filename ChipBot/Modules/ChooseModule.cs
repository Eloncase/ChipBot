﻿using ChipBot.Core.Implementations;
using Discord.Interactions;

namespace ChipBot.Modules
{
    [Group("choose", "choose from multiple variants")]
    public class ChooseModule : ChipInteractionModuleBase
    {
        public Random Random { get; set; }

        [SlashCommand("choose", "Picks a random option")]
        public async Task ChooseAsync([Summary("options", "Options divided with space")] string input)
        {
            var options = input.Split(" ");
            var selectedIndex = Random.Next(0, options.Length);
            var selected = options.ElementAt(selectedIndex);

            await RespondAsync(selected);
        }

        [SlashCommand("choosemany", "Picks a few random options")]
        public async Task ChooseManyAsync([Summary("count", "How many to pick")] int count, [Summary("options", "Options divided with space")] string input)
        {
            var options = input.Split(" ");
            if (count <= 0 || count >= options.Length)
            {
                await ReplyAsync("Specified amount is not correct.");
                return;
            }

            var list = new List<string>(options);
            var selected = new List<string>();

            for (var i = 0; i < count; i++)
            {
                var selectedIndex = Random.Next(0, list.Count);
                selected.Add(list.ElementAt(selectedIndex));
                list.RemoveAt(selectedIndex);
            }

            await RespondAsync(string.Join(", ", selected));
        }

        [SlashCommand("roll", "Test your luck")]
        public async Task RollAsync([Summary("min", "Min number to roll")] int min = 1, [Summary("max", "Max number to roll")] int max = 100)
            => await RespondAsync($"{Context.User.Mention} rolls {(new Random()).Next(min, max + 1)} ({min}-{max})");
    }
}
